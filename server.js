require('dotenv').config({ path: `${__dirname}/.env` });

const logger = require('morgan');
const cors = require('cors');
const path = require('path');

const express = require('express');
const mysql = require('mysql');
const bcrypt = require('bcrypt');

const saltRounds = 10;

const app = express();

const CREATE_USERS_TABLE_QUERY =
  'CREATE TABLE users(id int AUTO_INCREMENT, email VARCHAR(100), city VARCHAR(50), phone VARCHAR(50), PRIMARY KEY(id))';
const CREATE_TEMP_USERS_TABLE_QUERY =
  'CREATE TABLE temp_users(id int AUTO_INCREMENT, email VARCHAR(100), city VARCHAR(50), phone VARCHAR(50), PRIMARY KEY(id))';
const CREATE_ADMIN_USER_TABLE_QUERY =
  'CREATE TABLE admin(id int AUTO_INCREMENT, username VARCHAR(255), password VARCHAR(255), PRIMARY KEY(id))';
const SELECT_ALL_USERS_QUERY = 'SELECT * FROM users';
const SELECT_ADMIN_USER_QUERY = 'SELECT * FROM admin';

// Init vertexsms
// const vertexToken = process.env.VERTEX_TOKEN;

// const from = 'Vertex';
// const to = 'TO_NUMBER';
// const text = 'A text message sent using the Vertex SMS API';

// MySQL connection
const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

// Connect
db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log('=================================');
  console.log('MySql Connected...');
  console.log('=================================');
});

// Create DB
// GO TO localhost:5000/createdb
app.get('/createdb', (req, res) => {
  const sql = 'CREATE DATABASE tetatet';
  db.query(sql, (err) => {
    if (err) throw err;
    res.send('Database created...');
  });
});

// Create table
// GO TO http://localhost:5000/createuserstable
app.get('/createuserstable', (req, res) => {
  db.query(CREATE_USERS_TABLE_QUERY, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send('Users table created...');
  });
});

// Create temp users table
// GO TO http://localhost:5000/createtempuserstable
app.get('/createtempuserstable', (req, res) => {
  db.query(CREATE_TEMP_USERS_TABLE_QUERY, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send('Users temp table created...');
  });
});

// Create Admin user table
// GO TO http://localhost:5000/create-admin
app.get('/create-admin', (req, res) => {
  db.query(CREATE_ADMIN_USER_TABLE_QUERY, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send('Admin table created...');
  });
});

// Insert Admin test user
// GO TO http://localhost:5000/admin/add
app.get('/admin/add', (req, res) => {
  const username = 'admin';
  const password = 'admin';

  bcrypt.hash(password, saltRounds, (err, hash) => {
    const INSERT_ADMIN_USER_QUERY = 'INSERT INTO admin (username, password) VALUES(?, ?)';
    // Store hash in your password DB.
    db.query(INSERT_ADMIN_USER_QUERY, [username, hash], (err, result) => {
      if (err) throw err;
      console.log(result);
      res.send('Admin user added...');
    });
  });
});

// Get users
app.get('/users', (req, res) => {
  db.query(SELECT_ALL_USERS_QUERY, (err, result) => {
    if (err) throw err;
    res.json({
      users: result,
    });
  });
});

// Get admin user
app.get('/admin', (req, res) => {
  db.query(SELECT_ADMIN_USER_QUERY, (err, result) => {
    if (err) throw err;
    res.json({
      admin: result,
    });
  });
});

// Update admin user
app.get('/admin/update', (req, res) => {
  const { id, password } = req.query;

  bcrypt.hash(password, saltRounds, (err, hash) => {
    const UPDATE_ADMIN_PASSWORD_QUERY = `UPDATE admin SET password = '${hash}' WHERE id = ?`;

    db.query(UPDATE_ADMIN_PASSWORD_QUERY, [id], (err, result) => {
      console.log(result);

      if (err) throw err;
      res.send('Admin username updated...');
    });
  });
});

// Insert user
app.get('/users/add', (req, res) => {
  const { email, city, phone } = req.query;
  const INSERT_USER_QUERY = `INSERT INTO users (email, city, phone) VALUES('${email}', '${city}', '${phone}')`;

  db.query(INSERT_USER_QUERY, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send('User added...');

    // TODO: Vertex SMS request
  });
});

// Insert temp user
app.get('/temp-users/add', (req, res) => {
  const { email, city, phone } = req.query;
  const INSERT_TEMP_USER_QUERY = `INSERT INTO temp_users (email, city, phone) VALUES('${email}', '${city}', '${phone}')`;

  db.query(INSERT_TEMP_USER_QUERY, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send('Temp user added...');
  });
});

// Delete user
app.delete('/users/delete/:id', (req, res) => {
  const userId = req.params.id;
  const QUERY = `DELETE FROM users WHERE id = ${userId}`;
  db.query(QUERY, (err, result) => {
    if (err) throw err;
    res.send(JSON.stringify(result));
  });
});

// Middleware
if (!process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
}
app.use(cors({ credentials: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Error handling
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

// Server static assets if in production
if (process.env.NODE_ENV === 'production') {
  // Set static folder
  app.use(express.static('frontend/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html'));
  });
}

// Application port
const port = process.env.PORT || 5000;

// Start our server
app.listen(port);
console.log('=================================');
console.log(`Server is running on port ${port}`);
console.log('=================================');
