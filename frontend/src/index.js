import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import AppRouter from './components/routes/AppRouter';
// import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <BrowserRouter>
    <Route component={AppRouter} />
  </BrowserRouter>,
  document.getElementById('root')
);
