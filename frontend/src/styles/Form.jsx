import styled from 'styled-components';

export const Form = styled.form`
  grid-column: 2/3;
  grid-row: 2/3;
  justify-self: center;

  text-align: center;
  max-width: 37rem;
  padding-left: 2rem;
  padding-right: 2rem;

  @media (max-width: 31rem) {
    grid-column: 1/3;
  }
`;

export const FormTitle = styled.h1`
  font-size: 2.25rem;
  color: ${props => props.theme.white};
  text-transform: uppercase;

  @media (max-width: ${props => props.theme.maxWidth}) {
    font-size: 2.4rem;
    font-weight: 400;
  }
`;

export const FormSubTitle = styled.h1`
  font-size: 1rem;
  color: ${props => props.theme.white};

  @media (max-width: ${props => props.theme.maxWidth}) {
    font-size: 1.3rem;
    font-weight: 400;
  }
`;

export const CheckboxLabel = styled.h1`
  font-size: 0.8rem;
  color: ${props => props.theme.white};
  text-align: left;
  font-style: italic;
  @media (max-width: ${props => props.theme.maxWidth}) {
    font-size: 1.2rem;
  }
`;
export const UnderlineText = styled.span`
  text-decoration: underline;
  cursor: pointer;
`;

export const SuccessText = styled.h1`
  color: ${props => props.theme.white};
  text-align: center;
  font-size: 2rem;
`;

export const LoginContainer = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
`;

export const Button = styled.button`
  @font-face {
    font-family: 'Open Sans';
    src: url('/assets/fonts/OpenSans-ExtraBold.ttf') format('ttf');
    font-weight: normal;
    font-style: normal;
  }
  width: 100%;
  color: ${props => props.theme.white};
  background-color: #d9534f;
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 2rem;
  font-size: 1.1rem;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  cursor: pointer;
  background-image: none;
  text-transform: uppercase;
  outline: 0;

  border: 1px solid transparent;
  border-radius: 4px;
  border-color: #d43f3a;

  -ms-touch-action: manipulation;
  touch-action: manipulation;

  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  &.is-disabled {
    background-color: grey;
    border-color: grey;
    cursor: not-allowed;
  }
  &:focus {
    border: 2px solid transparent;
    background-color: #e0312c;
  }

  @media (max-width: ${props => props.theme.maxWidth}) {
    font-size: 1.1rem;
    height: 3.84rem;
  }
`;
