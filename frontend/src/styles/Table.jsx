import styled from 'styled-components';

export const TableTitle = styled.div`
  color: ${props => props.theme.white};
  font-size: 2rem;
  text-align: center;
`;

export const Table = styled.table`
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  color: ${props => props.theme.white};
  padding: 31px;
  margin: 0 auto;
  max-width: 60rem;
`;

export const TableTd = styled.td`
  border: 1px solid ${props => props.theme.grey};
  text-align: left;
  padding: 8px;
  color: ${props => props.theme.white};
`;

export const TableTh = styled.th`
  border: 1px solid ${props => props.theme.grey};
  text-align: left;
  padding: 8px;
  color: ${props => props.theme.white};
  font-size: 1rem;
`;

export const TableTr = styled.tr`
  &:hover {
    background-color: #4d4d4d;
    cursor: pointer;
  }
`;

export const Button = styled.button`
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 1rem;
  margin-top: 1rem;
  &.submit {
    background-color: #db0000;
  }
  &.download {
    background-color: ${props => props.theme.green};
    &:hover {
      background-color: ${props => props.theme.lightGreen};
    }
  }
  &.update-admin {
    background-color: #0a9f9f;
    margin-bottom: 1rem;
    margin-left: 6rem;
    &:hover {
      background-color: #058989;
    }
  }
  &:hover {
    background-color: #ee5757;
    cursor: pointer;
  }
`;
