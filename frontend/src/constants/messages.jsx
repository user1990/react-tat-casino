// Form
export const FORM_TITLE = 'Sveiki atvykę į Tete-A-Tete kazino';
export const FORM_SUB_TITLE =
  'Norėdami prisijungti prie Wi-Fi užpildykite žemiau esančius laukelius:';
export const CHECKBOX_RULES =
  'Prisijungdamas susipažinau su Wi-Fi naudojimosi taisyklėmis.';
export const CHECKBOX_DATA =
  'Sutinku, kad mano pateikti asmeniniai duomenys bus naudojami tiesioginės rinkodaros tikslais.';
export const SUBMIT = 'Prisijungti';
export const SUCCESS = 'Sėkmingai prisijungta!';
// Placeholders
export const EMAIL_PLACEHOLDER = 'Įveskite savo el. pašto adresą';
export const PHONE_PLACEHOLDER = 'Įveskite savo tel. nr.';
export const CITY_PLACEHOLDER = 'Įveskite savo miesto pavadinimą';
export const NAME_PLACEHOLDER = 'Įveskite savo admin vardą';
export const PASSWORD_PLACEHOLDER = 'Įveskite savo admin slaptažodį';
export const UPDATE_PASSWORD_PLACEHOLDER = 'Įveskite naują admin slaptažodį';
// Validation messages
export const INVALID_EMAIL = 'Suvestas el. paštas neatitinka *@*.* formato';
export const EMPTY_EMAIL_FIELD =
  'Laukelis privalomas. Įveskite savo el. pašto adresą';
export const EMPTY_PHONE_FIELD = 'Laukelis privalomas. Įveskite savo tel. nr.';
export const EMPTY_CITY_FIELD =
  'Laukelis privalomas. Įveskite savo miesto pavadinimą';
export const WIFI = 'Sutikimas su WiFi naudojimosi taisyklėmis yra privalomas';
export const INVALID_ADMIN_NAME = 'Neteisingas prisijungimo vardas';
export const INVALID_ADMIN_PASSWORD = 'Neteisingas prisijungimo slaptažodis';
// Admin panel
export const DOWNLOAD_DATA = 'Parsisiųsti duomenis';
export const DELETE_USER = 'Trinti';
export const EMAIL_FIELD = 'El. paštas';
export const PHONE_FIELD = 'Telefono numeris';
export const CITY_FIELD = 'Miestas';
export const TABLE_TITLE = 'Vartotojų panelė';
export const UPDATE_ADMIN = 'Pakeisti Admin slaptažodį';
export const UPDATE_ADMIN_TITLE = 'Admin slaptažodžio paketimas';
