import React from 'react';
import styled from 'styled-components';

export const FooterContainer = styled.footer`
  grid-column: 2/3;
  grid-row: 3/4;

  color: ${props => props.theme.lightRed};
  font-size: 0.8rem;
  text-align: center;
  padding-left: 2rem;
  padding-right: 2rem;

  @media (max-width: 31rem) {
    grid-column: 1/3;
  }
`;

const Footer = () => (
  <FooterContainer>
    <p>
      UAB "Tete-a-Tete" kazino yra pirmoji ir didžiausia lošimo automatų salonų
      bendrovė Lietuvoje, veikianti nuo 2003 m.
    </p>
    <p>© 2003 - 2018 "Tete-a-Tete" kazino. Visos teisės saugomos.</p>
  </FooterContainer>
);

export default Footer;
