import React from 'react';
import styled from 'styled-components';

export const TeteLogo = styled.img`
  grid-column: 1/2;
  grid-row: 1/2;
  justify-self: center;
  align-self: center;
`;

export const CbetLogo = styled.img`
  grid-column: 3/4;
  grid-row: 1/2;
  justify-self: center;
  align-self: center;

  @media (max-width: ${props => props.theme.maxWidth}) {
    grid-column: 2/3;
  }
`;

const Header = () => (
  <React.Fragment>
    <TeteLogo src="/assets/images/tat.png" alt="tete-logo" />
    <CbetLogo src="/assets/images/cbet.png" alt="cbet-logo" />
  </React.Fragment>
);

export default Header;
