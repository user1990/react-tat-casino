import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

export const Input = styled.input`
  margin-bottom: 0;
  display: block;
  width: 100%;
  min-width: 16rem;
  height: 3rem;
  padding: 6px 12px;
  font-size: 0.8rem;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  outline: 0;
  text-align: center;

  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);

  -webkit-transition: border-color ease-in-out 0.15s,
    -webkit-box-shadow ease-in-out 0.15s;
  transition: border-color ease-in-out 0.15s,
    -webkit-box-shadow ease-in-out 0.15s;
  transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
  transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s,
    -webkit-box-shadow ease-in-out 0.15s;
  &.is-invalid {
    border: 2px solid ${props => props.theme.red};
  }
  &::-webkit-input-placeholder {
    text-align: center;
  }

  &::-moz-placeholder {
    text-align: center;
  }
`;

export const InputError = styled.div`
  color: #a94442;
  text-align: left;
`;

const InputField = ({
  name,
  type,
  value,
  placeholder,
  error,
  onChange,
  onKeyPress,
  autoComplete,
}) => (
  <>
    <Input
      name={name}
      type={type}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      className={error ? 'is-invalid' : ''}
      onKeyPress={onKeyPress}
      autoComplete={autoComplete}
    />
    {error && <InputError>{error}</InputError>}
  </>
);

Input.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onKeyPress: PropTypes.func,
  autoComplete: PropTypes.string,
};

Input.defaultProps = {
  autoComplete: 'off',
};

export default InputField;
