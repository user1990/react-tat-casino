import React, { Component } from 'react';

import 'rc-checkbox/assets/index.css';

import InputField, { InputError } from '../common/InputField';
import {
  Form,
  FormTitle,
  Button,
  FormSubTitle,
  CheckboxLabel,
  SuccessText,
  UnderlineText,
} from '../../styles/Form';
import * as text from '../../constants/messages';
import { validateEmail } from '../../utils/Validation';
import ModalContent from './Modal';

export default class SubmitForm extends Component {
  constructor(props) {
    super(props);

    this.userOfTermsInput = React.createRef();
    this.userDataInput = React.createRef();
  }

  state = {
    users: [],
    email: '',
    city: '',
    phone: '',
    errors: {},
    isSubmit: false,
    isModalOpen: false,
    isDisabled: false,
    isTermsChecked: true,
    isDataChecked: true,
  };

  componentDidMount = () => {
    this.getUsers();
  };

  componentWillUpdate = () => {};

  onSubmit = e => {
    e.preventDefault();
    const { email, city, phone } = this.state;
    this.validateUser(email, city, phone);
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value, errors: {} });
  };

  onOpenModal = () => {
    this.setState({ isModalOpen: true });
  };

  onCloseModal = () => {
    this.setState({ isModalOpen: false });
  };

  getUsers = () => {
    fetch('/users')
      .then(res => res.json())
      .then(users => this.setState({ users }))
      .catch(err => console.log(err));
  };

  validateUser = (email, city, phone) => {
    // If input fields are empty
    if (email === '') {
      this.setState({
        errors: { email: text.EMPTY_EMAIL_FIELD },
      });
    }
    if (phone === '') {
      this.setState({
        errors: { phone: text.EMPTY_PHONE_FIELD },
      });
    }
    if (city === '') {
      this.setState({
        errors: { city: text.EMPTY_CITY_FIELD },
      });
    }
    if (email === '' && phone === '' && city === '') {
      this.setState({
        errors: {
          email: text.EMPTY_EMAIL_FIELD,
          city: text.EMPTY_CITY_FIELD,
          phone: text.EMPTY_PHONE_FIELD,
        },
      });
    }

    // If use of terms input is not checked
    if (!this.userOfTermsInput.current.checked) {
      this.setState({
        errors: {
          wifi: text.WIFI,
        },
      });
    }

    // Validate email format
    if (email.length >= 1 && !validateEmail(email)) {
      this.setState({
        errors: { email: text.INVALID_EMAIL },
      });
    }

    // If all fields completed add user data to the DB users table
    if (
      email !== '' &&
      phone !== '' &&
      city !== '' &&
      validateEmail(email) &&
      this.userOfTermsInput.current.checked &&
      this.userDataInput.current.checked
    ) {
      fetch(`/users/add?email=${email}&city=${city}&phone=${phone}`)
        .then(this.getUsers())
        .catch(err => console.log(err));
      this.setState({ isSubmit: true });

      // SEND SMS AND AF AFTER THAT ADD USER DATA TO DB
      // Ar nurodytas telefono numeris priklauso klientui išsiunčiant žinutę (pvz. vertexsms.com) ir reikalaujant įvesti atsiųstą kodą.
      /* const obj = {
          method: 'POST',
          headers: {
            'Content-type': 'application/json',
          },
          body: JSON.stringify({ "to": phone, "from": "invalid sender ID", "message": "Test SMS 001" }),

        }; */
      /* fetch('https://api.vertexsms.com/sms', obj)
        .then(res => console.log(res))
        .catch(err => console.log(err)); */
    }

    // If all fields completed and user not except add user data to the DB temp_users table
    if (
      phone !== '' &&
      city !== '' &&
      this.userOfTermsInput.current.checked &&
      !this.userDataInput.current.checked
    ) {
      fetch(`/temp-users/add?email=${email}&city=${city}&phone=${phone}`)
        .then(this.getUsers())
        .catch(err => console.log(err));
      this.setState({ isSubmit: true });
    }
  };

  handleKeyPress = e => {
    if (!/^\d*$/.test(e.key) || e.target.value.length >= 12) {
      e.preventDefault();
    }
  };

  handleCheck = () => {
    const useOfTermsValue =
      this.userOfTermsInput.current && this.userOfTermsInput.current.checked;
    const userDataValue =
      this.userDataInput.current && this.userDataInput.current.checked;

    this.setState({
      isTermsChecked: useOfTermsValue,
      isDataChecked: userDataValue,
    });

    if (useOfTermsValue === false && userDataValue === false) {
      this.setState({ isDisabled: true });
    }
    if (useOfTermsValue && userDataValue) {
      this.setState({ isDisabled: false });
    }
    if (useOfTermsValue) {
      this.setState({ isDisabled: false });
    }
    if (userDataValue) {
      this.setState({ isDisabled: false });
    }
  };

  render() {
    const {
      email,
      city,
      phone,
      errors,
      isSubmit,
      users, // eslint-disable-line no-unused-vars
      isModalOpen,
      isDisabled,
      isTermsChecked,
      isDataChecked,
    } = this.state;

    return (
      <Form>
        <FormTitle>{text.FORM_TITLE}</FormTitle>
        <FormSubTitle>{text.FORM_SUB_TITLE}</FormSubTitle>
        <InputField
          placeholder={text.EMAIL_PLACEHOLDER}
          type="email"
          name="email"
          value={email || ''}
          onChange={this.onChange}
          error={errors.email}
        />
        <br />
        <InputField
          placeholder={text.PHONE_PLACEHOLDER}
          name="phone"
          value={phone || ''}
          onChange={this.onChange}
          onKeyPress={this.handleKeyPress}
          error={errors.phone}
        />
        <br />
        <InputField
          placeholder={text.CITY_PLACEHOLDER}
          name="city"
          value={city || ''}
          onChange={this.onChange}
          error={errors.city}
        />
        <br />
        <CheckboxLabel>
          <input
            type="checkbox"
            name="checkbox"
            ref={this.userOfTermsInput}
            // value={useOfTermsValue || ''}
            onClick={this.handleCheck}
            defaultChecked={isTermsChecked}
          />
          &nbsp; Prisijungdamas susipažinau su Wi-Fi{' '}
          <UnderlineText onClick={this.onOpenModal}>
            naudojimosi taisyklėmis
          </UnderlineText>
        </CheckboxLabel>
        {errors && <InputError>{errors.wifi}</InputError>}
        <CheckboxLabel>
          <input
            type="checkbox"
            name="checkbox"
            ref={this.userDataInput}
            // value={userDataValue || ''}
            onClick={this.handleCheck}
            defaultChecked={isDataChecked}
          />
          &nbsp; {text.CHECKBOX_DATA}
        </CheckboxLabel>
        <Button
          type="submit"
          onClick={this.onSubmit}
          className={isDisabled ? 'is-disabled' : ''}
          disabled={isDisabled}
        >
          {text.SUBMIT}
        </Button>
        {isSubmit && <SuccessText>{text.SUCCESS}</SuccessText>}
        <ModalContent open={isModalOpen} onClose={this.onCloseModal} />
      </Form>
    );
  }
}
