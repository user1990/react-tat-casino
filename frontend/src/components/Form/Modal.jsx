import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';

import styles from './Modal.css';

const ModalContent = ({ open, onClose }) => (
  <Modal
    open={open}
    onClose={onClose}
    center
    classNames={{
      transitionEnter: styles.transitionEnter,
      transitionEnterActive: styles.transitionEnterActive,
      transitionExit: styles.transitionExitActive,
      transitionExitActive: styles.transitionExitActive,
    }}
    animationDuration={1000}
  >
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pulvinar
      risus non risus hendrerit venenatis. Pellentesque sit amet hendrerit
      risus, sed porttitor quam.
    </p>
  </Modal>
);

ModalContent.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalContent;
