import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Form, LoginContainer } from '../../styles/Form';
import InputField from '../common/InputField';
import * as text from '../../constants/messages';
import { Button } from '../../styles/Table';

export default class AdminPanelLogin extends Component {
  state = {
    username: '',
    password: '',
    errors: {},
    admin: {},
  };

  componentDidMount = () => {
    this.getAdmin();
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const { username, password } = this.state;

    this.validateAdmin(username, password);
  };

  getAdmin = () => {
    fetch('/admin')
      .then(res => res.json())
      .then(admin => this.setState({ admin }))
      .catch(err => console.log(err));
  };

  validateAdmin = (username, password) => {
    const { admin } = this.state;
    const { history } = this.props;
    const adminArr = admin.admin;
    const ADMIN_NAME = adminArr && adminArr[0].username;
    const ADMIN_PASSWORD = adminArr && adminArr[0].password;

    if (username !== ADMIN_NAME) {
      this.setState({
        errors: { name: text.INVALID_ADMIN_NAME },
      });
    }
    if (password !== ADMIN_PASSWORD) {
      this.setState({
        errors: { password: text.INVALID_ADMIN_PASSWORD },
      });
    }
    if (username !== ADMIN_NAME && password !== ADMIN_PASSWORD) {
      this.setState({
        errors: {
          name: text.INVALID_ADMIN_NAME,
          password: text.INVALID_ADMIN_PASSWORD,
        },
      });
    }
    if (username === ADMIN_NAME && password === ADMIN_PASSWORD) {
      localStorage.setItem('admin-password', ADMIN_PASSWORD);
      history.push(`/admin-panel/${username}`);
    }
  };

  render() {
    const { errors, username, password } = this.state;

    return (
      <LoginContainer>
        <Form>
          <InputField
            type="text"
            name="username"
            value={username}
            placeholder={text.NAME_PLACEHOLDER}
            onChange={this.onChange}
            error={errors.name}
          />
          <br />
          <InputField
            type="password"
            name="password"
            value={password}
            placeholder={text.PASSWORD_PLACEHOLDER}
            onChange={this.onChange}
            error={errors.password}
          />
          <Button type="submit" onClick={this.onSubmit} className="submit">
            {text.SUBMIT}
          </Button>
        </Form>
      </LoginContainer>
    );
  }
}

AdminPanelLogin.propTypes = {
  history: PropTypes.object,
};
