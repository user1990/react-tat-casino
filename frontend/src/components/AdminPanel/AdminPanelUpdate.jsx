import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import { Button, TableTitle } from '../../styles/Table';
import { Form, LoginContainer } from '../../styles/Form';
import InputField from '../common/InputField';
import * as text from '../../constants/messages';

export default class AdminPanelUpdate extends Component {
  state = {
    admin: {},
    password: '',
  };

  componentDidMount = () => {
    this.getAdmin();
  };

  onSubmit = e => {
    e.preventDefault();

    const { admin, password } = this.state;
    const { history } = this.props;

    const adminArr = admin.admin;
    const id = adminArr && adminArr[0].id;
    const username = adminArr && adminArr[0].username;

    fetch(`/admin/update?password=${password}&id=${id}`)
      .then(this.getAdmin())
      .then(history.push(`/admin-panel/${username}`))
      .catch(err => console.log(err));
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getAdmin = () => {
    fetch('/admin')
      .then(res => res.json())
      .then(admin => this.setState({ admin }))
      .catch(err => console.log(err));
  };

  render() {
    const { admin, password } = this.state;

    const adminArr = admin.admin;
    const username = adminArr && adminArr[0].username;

    return (
      <>
        <TableTitle>{text.UPDATE_ADMIN_TITLE} </TableTitle>

        <LoginContainer>
          <Form>
            <InputField
              type="password"
              name="password"
              value={password}
              placeholder={text.UPDATE_PASSWORD_PLACEHOLDER}
              onChange={this.onChange}
            />
            <Link to={`/admin-panel/${username}`}>
              <Button type="submit" onClick={this.onSubmit} className="submit">
                PAKEISTI
              </Button>
            </Link>
          </Form>
        </LoginContainer>
      </>
    );
  }
}

AdminPanelUpdate.propTypes = {
  history: PropTypes.object,
};
