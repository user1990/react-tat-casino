import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import CsvDownloader from 'react-csv-downloader';

import * as text from '../../constants/messages';
import {
  Button,
  Table,
  TableTitle,
  TableTr,
  TableTd,
  TableTh,
} from '../../styles/Table';

export default class AdminPanel extends Component {
  state = {
    users: [],
    admin: {},
  };

  componentDidMount = () => {
    this.getUsers();
    this.getAdmin();
  };

  getUsers = () => {
    fetch('/users')
      .then(res => res.json())
      .then(users => this.setState({ users }))
      .catch(err => console.log(err));
  };

  getAdmin = () => {
    fetch('/admin')
      .then(res => res.json())
      .then(admin => this.setState({ admin }))
      .catch(err => console.log(err));
  };

  handleDeleteUser = userId => {
    const requestOptions = {
      method: 'DELETE',
    };

    fetch(`/users/delete/${userId}`, requestOptions)
      .then(res => {
        if (res.status >= 400) {
          throw new Error('Bad response from server');
        }
        this.getUsers();
        return res.json();
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    const { users, admin } = this.state;

    const adminArr = admin.admin;
    const username = adminArr && adminArr[0].username;

    const usersArr = users.users;

    const renderUsers =
      usersArr &&
      usersArr.map(user => (
        <TableTr key={user.id}>
          <TableTd>{user.email}</TableTd>
          <TableTd>{user.city}</TableTd>
          <TableTd>{user.phone}</TableTd>
          <TableTd>
            <Button
              type="button"
              onClick={() => this.handleDeleteUser(user.id)}
              className="submit"
            >
              {text.DELETE_USER}
            </Button>
          </TableTd>
        </TableTr>
      ));

    const columns = [
      {
        id: 'first',
        displayName: 'El. paštas',
      },
      {
        id: 'second',
        displayName: 'Miestas',
      },
      {
        id: 'third',
        displayName: 'Telefono numeris',
      },
    ];

    const usersCSVdata =
      usersArr &&
      usersArr.map(user => ({
        first: user.email,
        second: user.city,
        third: user.phone,
        fourth: user.agreed,
      }));

    const renderTable = (
      <>
        <TableTitle>
          {text.TABLE_TITLE}{' '}
          <Link to={`/admin-panel/${username}/update`}>
            <Button type="button" className="update-admin">
              {text.UPDATE_ADMIN}
            </Button>
          </Link>
        </TableTitle>
        <Table>
          <tbody>
            <TableTr>
              <TableTh>{text.EMAIL_FIELD}</TableTh>
              <TableTh>{text.CITY_FIELD}</TableTh>
              <TableTh>{text.PHONE_FIELD}</TableTh>
              <TableTh>
                <CsvDownloader
                  filename="vartotoju_duomenys"
                  separator=";"
                  columns={columns}
                  datas={usersCSVdata || []}
                >
                  <Button type="button" className="download">
                    {text.DOWNLOAD_DATA}
                  </Button>
                </CsvDownloader>
              </TableTh>
            </TableTr>
            {renderUsers}
          </tbody>
        </Table>
      </>
    );

    return <>{renderTable}</>;
  }
}
