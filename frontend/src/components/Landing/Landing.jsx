import React, { Component } from 'react';
import styled from 'styled-components';
import Form from '../Form/Form';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

export const LandingContainer = styled.div`
  display: grid;
  grid-template-columns: 2fr 5fr 2fr;
  grid-template-rows: 6rem 1fr 6rem;

  @media (max-width: ${props => props.theme.maxWidth}) {
    grid-template-columns: repeat(2, 1fr);
  }
`;

export default class Landing extends Component {
  render() {
    return (
      <LandingContainer>
        <Header />
        <Form />
        <Footer />
      </LandingContainer>
    );
  }
}
