import React from 'react';
import { Route } from 'react-router-dom';
import { ThemeProvider, createGlobalStyle } from 'styled-components/macro';

import Landing from '../Landing/Landing';
import AdminPanel from '../AdminPanel/AdminPanel';
import AdminPanelLogin from '../AdminPanel/AdminPanelLogin';
import AdminPanelUpdate from '../AdminPanel/AdminPanelUpdate';

const tatTheme = {
  red: ' #a94442',
  darkRed: '#360908',
  green: 'green',
  lightGreen: ' #08b508',
  lightRed: '#c13d3a',
  black: '#393939',
  white: '#fff',
  grey: '#dddddd',
  maxWidth: '31rem',
};

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Open Sans';
    src: url('/assets/fonts/OpenSans-Regular.ttf') format('ttf');
    font-weight: normal;
    font-style: normal;
  }
  html {
    box-sizing: border-box;
  }
  *,
  *:after,
  *:before {
    -webkit-box-sizing: inherit;
    box-sizing: inherit;
  }
  body {
    margin: 0;
    padding: 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 0.875rem;
    line-height: 1.42857143;
    color: #333;
    background-color: ${props => props.theme.darkRed};
  }
  a {
    text-decoration: none;
    color: ${props => props.theme.black};
  }
`;

const AppRouter = () => (
  <ThemeProvider theme={tatTheme}>
    <div>
      <Route exact path="/" component={Landing} />
      <Route exact path="/admin-panel" component={AdminPanelLogin} />
      <Route exact path="/admin-panel/:username" component={AdminPanel} />
      <Route
        exact
        path="/admin-panel/:username/update"
        component={AdminPanelUpdate}
      />
      <GlobalStyle />
    </div>
  </ThemeProvider>
);

export default AppRouter;
