# TaT Kazino

---

React Full stack application for users submission and display them in Admin Panel

### Main Technologies

---

#### Client Side

- [x] **[React](https://github.com/facebook/react)**
- [x] **[Styled Components](https://github.com/styled-components/styled-components)**
- [x] **[React-Router](https://github.com/ReactTraining/react-router)**

#### Server Side

- [x] **[Node.js / Express](https://github.com/expressjs/express)**
- [x] **[MySQL](https://github.com/mysql)**

### Setup

---

1. Clone repository

```bash
git clone https://github.com/user1990/devconnector-react-app.git
```

2. Go to project direction

3. Install dependencies

```bash
npm i
cd frontend/
npm i
```

4. Run app

```bash
go to project root directory
npm run dev
http://localhost:3000/
```

```bash
http://localhost:3000/admin-panel
```

5. For MySQL database connection create `backend/.env` file and add you credentials, example:

```bash
DB_HOST=[DATABASE_HOST]
DB_USER=[DATABASE_USER]
DB_PASSWORD=[DATABASE_PASSWORD]
DB_NAME=[DATABASE_NAME]
```

6. Test sending SMS via vertexSMS. In same `backend/.env` file add you vertexsms token, example:

```bash
VERTEX_TOKEN=[YOU_VERTEX_ACCESS_TOKEN]
```
